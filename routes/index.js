'use strict'

const express = require('express');
const api = express.Router();
const auth = require('../middlewares/auth');
const userCtrl = require('../controllers/user');
const agentCtrl = require('../controllers/agent');
const User = require('../models/user');


// Rutas API
api.get('/user/:user', (req, res) => {
    let user_id = req.params.user;

    User.findById(user_id, (err, user) => {

        if (err) res.status(500).send({message: `Error de petición: ${err}`})
        if (!user) res.status(404).send({message: `No existe el usuario`})

        res.status(200).render('mapa', {
            "mensaje": `Hola ${req.params.name}`
        });
    });
});

api.get('/login', (req, res) => {
    res.status(200).render('login')
});



api.post('/registro', userCtrl.registro)
api.post('/login', userCtrl.login)

api.get('/', (req, res) => {
    res.status(200).render('testbed')
});

api.get('/city', (req, res) => {
    res.status(200).render('city')
});

api.get('/private', auth, function(req, res){
    res.status(200).send({message: "tienes acceso!"})
});


api.get('/testbed', (req, res) => {
    res.status(200).render("testbed")
});

api.get('/dashboard', (req, res) => {
    res.status(200).render("dashboard")
});


api.post('/agent_active', agentCtrl.agent_active);

api.get('/agente/info/:agente_id', agentCtrl.info);


module.exports = api;